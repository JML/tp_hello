package com.example.tphelloworld;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/taches")

public class FormulaireController {


    private final List<Tache> tacheList = new ArrayList<>();

    @GetMapping
    public String getTaches(Model model) {
        model.addAttribute("taches", tacheList);
        return "liste-taches";
    }

    @GetMapping
    public String afficherFormulaire(Model model){
        Tache tache = new Tache();
        model.addAttribute("tache",tache);
        return "formulaire";
    }

    @PostMapping
    public String creationTache(@ModelAttribute("tache") Tache tache) {
        tacheList.add(tache);
        return "redirect:/taches";
    }

}
